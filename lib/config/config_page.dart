import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertestsearchapp/config/config_bloc.dart';
import 'package:fluttertestsearchapp/config/config_state.dart';
import 'package:fluttertestsearchapp/data/rest/service/search_service.dart';
import 'package:fluttertestsearchapp/ui/pages/home/home_page.dart';
import 'package:fluttertestsearchapp/utils/my_colors.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';
import 'package:provider/provider.dart';

class ConfigPage extends StatefulWidget {
  @override
  _ConfigPageState createState() => _ConfigPageState();
}

class _ConfigPageState extends State<ConfigPage> {
  ConfigBloc configBloc;

  @override
  void dispose() {
    configBloc.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    setupApp();
  }

  setupApp() {
    configBloc = ConfigBloc();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<SearchService>(
          create: (_) => SearchService.createApi(),
          dispose: (_, SearchService service) => service.client.dispose(),
        ),
        BlocProvider(
          create: (context) => configBloc,
        )
      ],
      child: BlocBuilder<ConfigBloc, ConfigState>(
        builder: (_, state) {
          return MaterialApp(
            theme: ThemeData(
              primaryColor: MyColors.primaryColor,
              cursorColor: MyColors.blackTextColor,
              fontFamily: MyFont.roboto,
            ),
            debugShowCheckedModeBanner: false,
            title: "Test App",
            home: MainPage(),
            // ignore: missing_return
            onGenerateRoute: (RouteSettings settings) {
              switch (settings.name) {
                case HomePage.ROUTE_NAME:
                  return CupertinoPageRoute(
                      builder: (_) => HomePage(), settings: settings);
              }
            },
          );
        },
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HomePage();
  }
}
