import 'package:equatable/equatable.dart';
import 'package:fluttertestsearchapp/config/config_bloc.dart';
import 'package:fluttertestsearchapp/config/config_state.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ConfigEvent extends Equatable {
  ConfigEvent([List props = const <dynamic>[]]) : super(props);

  Future<ConfigState> applyAsync({ConfigState currentState, ConfigBloc bloc});
}