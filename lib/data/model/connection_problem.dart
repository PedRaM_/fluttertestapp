class ConnectionProblem {
  String message;
  int id;

  ConnectionProblem({
    this.message = "Connection error",
    this.id,
  });
}
