class Product {
  int id;
  String title;
  String price;
  int userId;
  String createDate;
  String updateDate;

  Product._();

  Product({
    this.id,
    this.title,
    this.price,
    this.userId,
    this.createDate,
    this.updateDate,
  });

  factory Product.fromJson(body) => Product(
        id: body['id'] ?? 0,
        title: body['title'] ?? "",
        price: body['price'] ?? "0",
        userId: body['user_id'] ?? 0,
        createDate: body['created_at'] ?? "",
        updateDate: body['updated_at'] ?? "",
      );
}
