import 'package:fluttertestsearchapp/data/rest/model/search/category.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/video/product.dart';

class CategoryVideo {
  int id;
  String title;
  String videoSource;
  String description;
  String format;
  bool isMain;
  bool isFollowed;
  bool isBought;
  int viewCount;
  double rate;
  List<String> tags;
  Product product;
  Category category;

  CategoryVideo._();

  CategoryVideo({
    this.id,
    this.title,
    this.videoSource,
    this.description,
    this.format,
    this.isMain,
    this.isFollowed,
    this.isBought,
    this.viewCount,
    this.rate,
    this.tags,
    this.product,
    this.category,
  });

  factory CategoryVideo.fromJson(body) {
    List<dynamic> tagsDynamic = body['tags'] ?? List();
    List<String> tags = List();
    for (dynamic item in tagsDynamic) {
      String tag = item;
      tags.add(tag);
    }
    return CategoryVideo(
      id: body['id'] ?? 0,
      title: body['title'] ?? "",
      videoSource: body['source'] ?? "",
      description: body['description'] ?? "",
      format: body['format'] ?? "",
      isMain: body['is_main'] ?? false,
      isFollowed: body['is_followed'] ?? false,
      isBought: body['is_bought'] ?? false,
      viewCount: body['view_count'] ?? 0,
      rate: double.tryParse(body['rate'] ?? "0.0") ?? 0.0,
      tags: tags,
      product: Product.fromJson(body['product']),
      category: Category.fromJson(body['category']),
    );
  }

  List<CategoryVideo> fromJsonList(List<dynamic> body) {
    List<CategoryVideo> videos = List();
    for (dynamic item in body) {
      CategoryVideo category = CategoryVideo.fromJson(item);
      videos.add(category);
    }
    return videos;
  }
}
