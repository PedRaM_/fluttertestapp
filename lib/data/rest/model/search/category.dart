class Category {
  int id;
  String title;
  int viewCount;

  Category._();

  Category({this.id, this.title, this.viewCount});

  factory Category.fromJson(dynamic body) => Category(
        id: body['id'] ?? 0,
        title: body['title'] ?? "",
        viewCount: body['view_count'] ?? 0,
      );

  List<Category> fromJsonList(List<dynamic> body) {
    List<Category> categories = List();
    for (dynamic item in body) {
      Category category = Category.fromJson(item);
      categories.add(category);
    }
    return categories;
  }
}
