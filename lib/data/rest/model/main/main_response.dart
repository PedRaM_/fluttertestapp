import 'dart:convert' as converter;

import 'package:chopper/chopper.dart';
import 'package:fluttertestsearchapp/data/rest/model/main/pagination.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/category.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/video/video.dart';

class MainResponse<T> {
  Pagination pagination;

  List<T> data;

  MainResponse._();

  MainResponse({this.pagination, this.data});

  factory MainResponse.fromCategoryResponse(Response<dynamic> response) {
    dynamic body = converter.json.decode(response.body);
    dynamic pagination = body['pagination'];
    List<dynamic> data = body['data'] ?? List();
    Pagination pag;
    if (pagination != null) pag = Pagination.fromJson(pagination);
    dynamic categories = Category().fromJsonList(data);
    return MainResponse(pagination: pag, data: categories);
  }

  factory MainResponse.fromCategoryVideoResponse(Response<dynamic> response) {
    dynamic body = converter.json.decode(response.body);
    dynamic pagination = body['pagination'];
    List<dynamic> data = body['data'] ?? List();
    Pagination pag;
    if (pagination != null) pag = Pagination.fromJson(pagination);
    dynamic videos = CategoryVideo().fromJsonList(data);
    return MainResponse(pagination: pag, data: videos);
  }
}
