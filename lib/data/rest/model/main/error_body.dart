import 'package:chopper/chopper.dart';

class ErrorBody {
  String message;

  ErrorBody({this.message});

  ErrorBody.fromJson(Response<dynamic> json) {
    message = 'An error occurred';
  }
}
