class Pagination {
  int currentPage;
  int total;
  int lastPage;
  int perPage;

  Pagination._();

  Pagination({this.currentPage, this.total, this.lastPage, this.perPage});

  factory Pagination.fromJson(dynamic body) => Pagination(
        currentPage: body['current_page'] ?? 0,
        lastPage: body['last_page'] ?? 0,
        total: body['total'] ?? 0,
        perPage: body['per_page'] ?? 0,
      );
}
