import 'package:chopper/chopper.dart';
import 'package:fluttertestsearchapp/utils/rest_utils.dart';

part 'search_service.chopper.dart';

@ChopperApi()
abstract class SearchService extends ChopperService {
  @Get(path: RestUtils.CATEGORY)
  Future<Response> getCategories();

  @Get(path: RestUtils.CATEGORY_VIDEO)
  Future<Response> getVideos(@Path("id") int id);

  static SearchService createApi() {
    final client = ChopperClient(
      baseUrl: RestUtils.getApiBaseUrl(),
      services: [
        _$SearchService(),
      ],
    );
    return _$SearchService(client);
  }
}
