import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/ui/components/text/regular_text.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/category/category_fragment.dart';
import 'package:fluttertestsearchapp/ui/pages/home/home_contract.dart';
import 'package:fluttertestsearchapp/utils/my_colors.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';

class HomePage extends StatefulWidget {
  static const String ROUTE_NAME = "HomePage";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> implements HomeContract {
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  int _bottomNavigationCurrentIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _bottomNavigationCurrentIndex,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedIconTheme:
            IconThemeData(color: MyColors.primaryColor, size: 24),
        unselectedIconTheme:
            IconThemeData(color: MyColors.textHintColor, size: 24),
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              title: RegularText(),
              icon: Icon(
                MyFontIcon.home,
              )),
          BottomNavigationBarItem(
              title: RegularText(),

              icon: Icon(
            MyFontIcon.search,
          )),
          BottomNavigationBarItem(
              title: RegularText(),

              icon: Icon(
            MyFontIcon.add,
          )),
          BottomNavigationBarItem(
              title: RegularText(),

              icon: Icon(
            MyFontIcon.download,
          )),
          BottomNavigationBarItem(
              title: RegularText(),
              icon: Icon(
            MyFontIcon.avatar,
          )),
        ],
        onTap: (index) {
          onBottomMenuItemClick(index);
        },
        selectedItemColor: MyColors.primaryColor,
        backgroundColor: Colors.white,
        unselectedItemColor: MyColors.textHintColor,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      body: _bottomNavigationCurrentIndex == 1 ? SearchFragment(scaffoldKey: _scaffoldKey,) : Container(),
    );
  }

  @override
  void onBottomMenuItemClick(int index) {
    if (mounted)
      setState(() {
        _bottomNavigationCurrentIndex = index;
      });
  }
}
