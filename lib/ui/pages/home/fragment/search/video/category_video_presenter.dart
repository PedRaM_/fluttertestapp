import 'package:fluttertestsearchapp/data/rest/model/main/main_response.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/video/video.dart';
import 'package:fluttertestsearchapp/data/rest/service/search_service.dart';
import 'package:fluttertestsearchapp/presenter/main_presenter.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/video/category_video_contract.dart';

class CategoryVideoPresenter extends MainPresenter {
  CategoryVideoContract _view;

  CategoryVideoPresenter(this._view);

  void getCategoryVideos(SearchService service, int page, int id) {
    callApi(
      api: service.getVideos(id),
      id: 1,
      onFail: (error) => _view.onFail(error),
      onResponseFail: (error) => _view.onGetCategoryVideosFail(error),
      onResponse: (response) => _view.onGetCategoryVideos(
          MainResponse<CategoryVideo>.fromCategoryVideoResponse((response))),
    );
  }
}
