import 'package:fluttertestsearchapp/contracts/fail_contract.dart';
import 'package:fluttertestsearchapp/data/rest/model/main/main_response.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/category.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/video/video.dart';

mixin CategoryVideoContract on FailContract {

  void getCategoryVideos(int id);

  void onGetCategoryVideos(MainResponse<CategoryVideo> response);

  void onGetCategoryVideosFail(error);
}
