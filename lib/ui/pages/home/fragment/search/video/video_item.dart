import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/video/video.dart';
import 'package:fluttertestsearchapp/ui/components/loading/loading.dart';
import 'package:fluttertestsearchapp/ui/components/text/regular_text.dart';
import 'package:fluttertestsearchapp/utils/my_colors.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';
import 'package:video_player/video_player.dart';

class VideoItem extends StatefulWidget {
  final CategoryVideo video;
  final Size size;

  const VideoItem({Key key, this.video, this.size}) : super(key: key);

  @override
  _VideoItemState createState() => _VideoItemState();
}

class _VideoItemState extends State<VideoItem> {
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.video.videoSource)
      ..initialize().then((_) {
        if(mounted)
        setState(() {});
      });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(0.5),
      child: Stack(
        children: [
          _controller.value.initialized
              ? Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: VideoPlayer(_controller),
                )
              : Center(child: Loading()),
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
                  Color(0xFF000000),
                  Color(0x33000000),
                  Color(0x08000000),
                ],
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  RegularText(
                    text: "${widget.video.description}",
                    color: MyColors.priceColor,
                    fontWeight: MyFont.regular,
                    maxLine: 2,
                    fontSize: 14,
                  ),
                  SizedBox(height: 6),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      RegularText(
                        text: "${widget.video.product.price}\$",
                        color: MyColors.priceColor,
                        fontWeight: MyFont.bold,
                        fontSize: 13,
                      ),
                      Row(
                        children: [
                          Icon(
                            MyFontIcon.star,
                            size: 12,color: MyColors.priceColor,
                          ),
                          SizedBox(width: 4,),
                          RegularText(
                            text: "${widget.video.rate}",
                            color: MyColors.priceColor,
                            fontWeight: MyFont.regular,
                            fontSize: 12,
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
