import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/data/rest/model/main/main_response.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/category.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/video/video.dart';
import 'package:fluttertestsearchapp/data/rest/service/search_service.dart';
import 'package:fluttertestsearchapp/ui/components/emptystate/empty_state.dart';
import 'package:fluttertestsearchapp/ui/components/error/error_handler.dart';
import 'package:fluttertestsearchapp/ui/components/loading/loading.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/video/category_video_contract.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/video/category_video_presenter.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/video/video_item.dart';
import 'package:provider/provider.dart';

class VideoItemFragment extends StatefulWidget {
  final Size size;
  final Category category;
  final GlobalKey<ScaffoldState> scaffoldKey;

  const VideoItemFragment({Key key, this.size, this.category, this.scaffoldKey})
      : super(key: key);

  @override
  _VideoItemFragmentState createState() => _VideoItemFragmentState();
}

class _VideoItemFragmentState extends State<VideoItemFragment>
    with AutomaticKeepAliveClientMixin
    implements CategoryVideoContract {
  _VideoItemFragmentState() {
    _presenter = CategoryVideoPresenter(this);
  }

  CategoryVideoPresenter _presenter;
  bool _videoLoading = false;
  int _videoPage = 1;
  MainResponse<CategoryVideo> _videoResponse = MainResponse<CategoryVideo>();

  @override
  void initState() {
    super.initState();
    getCategoryVideos(widget.category.id);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _videoLoading
        ? Center(child: Loading())
        : SizedBox(
            width: double.infinity,
            height: double.infinity,
            child:
                _videoResponse.data == null || _videoResponse.data.length == 0
                    ? EmptyState(
                        message:
                            "No video for this category, please select another one.",
                      )
                    : GridView.count(
                        padding: EdgeInsets.only(top: 8),
                        crossAxisCount: 2,
                        childAspectRatio: 0.65,
                        children: List.generate(
                          _videoResponse.data.length,
                          (index) => VideoItem(
                            size: widget.size,
                            video: _videoResponse.data[index],
                          ),
                        ),
                      ),
          );
  }

  @override
  void getCategoryVideos(int id) {
    if (mounted)
      setState(() {
        _videoLoading = true;
      });
    _presenter.getCategoryVideos(
        Provider.of<SearchService>(context, listen: false), _videoPage, id);
  }

  @override
  void onFail(error) {
    print(error);
    if (mounted)
      setState(() {
        _videoLoading = false;
      });
    ErrorHandler(error: error, scaffoldKey: widget.scaffoldKey).handle();
  }

  @override
  void onGetCategoryVideos(MainResponse<CategoryVideo> response) {
    if (mounted)
      setState(() {
        _videoLoading = false;
      });
    _videoResponse = response;
  }

  @override
  void onGetCategoryVideosFail(error) {
    if (mounted)
      setState(() {
        _videoLoading = false;
      });
    ErrorHandler(error: error, scaffoldKey: widget.scaffoldKey).handle();
  }

  @override
  bool get wantKeepAlive => true;
}
