import 'package:fluttertestsearchapp/data/rest/model/main/main_response.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/category.dart';
import 'package:fluttertestsearchapp/data/rest/service/search_service.dart';
import 'package:fluttertestsearchapp/presenter/main_presenter.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/category/category_contract.dart';

class CategoryPresenter extends MainPresenter {
  CategoryContract _view;

  CategoryPresenter(this._view);

  void getCategories(SearchService service, int page) {
    callApi(
      api: service.getCategories(),
      id: 1,
      onFail: (error) => _view.onFail(error),
      onResponseFail: (error) => _view.onGetCategoriesFail(error),
      onResponse: (response) => _view.onGetCategories(
          MainResponse<Category>.fromCategoryResponse((response))),
    );
  }
}
