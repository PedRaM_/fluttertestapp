import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/data/rest/model/main/main_response.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/category.dart';
import 'package:fluttertestsearchapp/data/rest/service/search_service.dart';
import 'package:fluttertestsearchapp/ui/components/error/error_handler.dart';
import 'package:fluttertestsearchapp/ui/components/input/material_text_field.dart';
import 'package:fluttertestsearchapp/ui/components/loading/loading.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/category/category_contract.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/category/category_presenter.dart';
import 'package:fluttertestsearchapp/ui/pages/home/fragment/search/video/category_video_fragment.dart';
import 'package:fluttertestsearchapp/utils/my_colors.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';
import 'package:provider/provider.dart';

class SearchFragment extends StatefulWidget {
  final scaffoldKey;

  const SearchFragment({Key key, this.scaffoldKey}) : super(key: key);

  @override
  _SearchFragmentState createState() => _SearchFragmentState();
}

class _SearchFragmentState extends State<SearchFragment>
    with TickerProviderStateMixin
    implements CategoryContract {
  _SearchFragmentState() {
    _presenter = CategoryPresenter(this);
  }

  CategoryPresenter _presenter;

  static const double TAB_HEIGHT = 28;
  static const double INPUT_SIZE = 48;
  static const double INPUT_MARGIN = 24;
  static const double TOP_HEIGHT = TAB_HEIGHT + INPUT_SIZE + INPUT_MARGIN;

  var _searchController = new TextEditingController();
  var _searchFocusNode = new FocusNode();

  MainResponse<Category> _categoryResponse = MainResponse<Category>();

  Size _size;
  bool _loading = false;
  bool _searchLoading = false;
  bool _videoLoading = false;
  bool _hasMore = true;
  bool _itemLoading = false;
  String _searchQuery = "";
  int _categoryPage = 1;
  TabController _tabController;
  int _selectedPage;
  int _tabIndex = 0;

  @override
  void initState() {
    super.initState();
    getCategories(false);
  }

  @override
  void dispose() {
    if (_tabController != null) _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: _loading
          ? Center(child: Loading())
          : SafeArea(
              child: Stack(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 16, right: 16,top: 8),
                    child: SizedBox(
                      height: INPUT_SIZE,
                      child: MaterialTextField(
                        controller: _searchController,
                        focusNode: _searchFocusNode,
                        hint: "Tab to search",
                        textInputAction: TextInputAction.search,
                        maxLine: 1,
                        onSubmit: (text) {
                          //todo do search
                        },
                        textAlignVertical: TextAlignVertical.center,
                        icon: Icon(
                          MyFontIcon.search,
                          color: MyColors.textHintColor,
                          size: 16,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(top: INPUT_SIZE + INPUT_MARGIN),
                    height: TAB_HEIGHT,
                    width: _size.width,
                    child: TabBar(
                      controller: _tabController,
                      indicatorColor: MyColors.primaryColor,
                      indicatorWeight: 2.5,
                      isScrollable: true,
                      indicatorSize: TabBarIndicatorSize.label,
                      labelColor: MyColors.blackTextColor,
                      labelStyle: TextStyle(
                        fontSize: 14,
                        fontFamily: MyFont.roboto,
                        fontWeight: MyFont.bold,
                      ),
                      unselectedLabelColor: MyColors.textHintColor,
                      unselectedLabelStyle: TextStyle(
                        fontSize: 14,
                        fontFamily: MyFont.roboto,
                        fontWeight: MyFont.bold,
                      ),
                      tabs: [
                        if (_categoryResponse.data != null)
                          for (Category category in _categoryResponse.data)
                            Tab(
                              text: category.title,
                            )
                      ],
                    ),
                  ),
                  Container(
                    width: _size.width,
                    height: double.infinity,
                    margin: EdgeInsets.only(top: TOP_HEIGHT),
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        if (_categoryResponse.data != null)
                          for (Category category in _categoryResponse.data)
                            VideoItemFragment(
                              category: category,
                              scaffoldKey: widget.scaffoldKey,
                              size: _size,
                            ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  @override
  void initTabController() {
    _tabController = TabController(
        length: _categoryResponse.data.length,
        vsync: this,
        initialIndex: _tabIndex);
    _tabController.addListener(() {
      _tabIndex = _tabController.index;
    });
  }

  @override
  void getCategories(bool needSetState) {
    if (needSetState && mounted)
      setState(() {
        _loading = true;
        _videoLoading = true;
      });
    else {
      _loading = true;
      _videoLoading = true;
    }
    _presenter.getCategories(
        Provider.of<SearchService>(context, listen: false), _categoryPage);
  }

  @override
  void onFail(error) {
    print(error);
    if (mounted)
      setState(() {
        _loading = false;
        _itemLoading = false;
        _searchLoading = false;
        _videoLoading = false;
      });
    ErrorHandler(error: error, scaffoldKey: widget.scaffoldKey).handle();
  }

  @override
  void onGetCategories(MainResponse<Category> response) {
    if (mounted)
      setState(() {
        _categoryResponse = response;
        initTabController();
        _loading = false;
      });
  }

  @override
  void onGetCategoriesFail(error) {
    if (mounted)
      setState(() {
        _loading = false;
      });
    ErrorHandler(error: error, scaffoldKey: widget.scaffoldKey).handle();
  }
}
