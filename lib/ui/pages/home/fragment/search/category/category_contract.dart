import 'package:fluttertestsearchapp/contracts/fail_contract.dart';
import 'package:fluttertestsearchapp/data/rest/model/main/main_response.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/category.dart';
import 'package:fluttertestsearchapp/data/rest/model/search/video/video.dart';

mixin CategoryContract on FailContract {

  void initTabController();

  void getCategories(bool needSetState);

  void onGetCategories(MainResponse<Category> response);

  void onGetCategoriesFail(error);
}
