import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/utils/my_colors.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';

class MaterialTextField extends StatelessWidget {
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final TextInputAction textInputAction;
  final String errorText;
  final TextEditingController controller;
  final String hint;
  final double fontSize;
  final double hintFontSize;
  final Function(String) onSubmit;
  final FontWeight fontWeight;
  final TextInputType inputType;
  final Color hintColor;
  final Color textColor;
  final int maxLength;
  final Function(String) onChange;
  final int maxLine;
  final Widget icon;
  final TextAlignVertical textAlignVertical;

  const MaterialTextField({
    Key key,
    this.focusNode,
    this.errorText = "",
    this.nextFocusNode,
    this.textInputAction,
    this.hint = "",
    this.controller,
    this.fontSize = 15,
    this.hintFontSize = 16,
    this.fontWeight = FontWeight.w400,
    this.inputType = TextInputType.visiblePassword,
    this.hintColor = MyColors.textHintColor,
    this.textColor = MyColors.blackTextColor,
    this.maxLength = 50,
    this.onChange,
    this.icon,
    this.maxLine = 1,
    this.textAlignVertical = TextAlignVertical.center,
    this.onSubmit,
  }) : super(key: key);

  _errorSection({String error = ""}) => Positioned(
        bottom: 6,
        left: 10,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.only(left: 4, right: 4),
            child: Text(
              error,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: MyColors.redColor,
                fontFamily: MyFont.roboto,
                fontSize: 9,
                fontWeight: MyFont.regular,
              ),
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Color(0xFFEEF1F6),
          ),
          child: TextField(
            cursorColor: MyColors.primaryColor,
            cursorRadius: Radius.circular(16),
            cursorWidth: 2,
            keyboardType: inputType,
            focusNode: focusNode,
            onChanged: (text) {
              if (onChange != null) onChange(text);
            },
            onSubmitted: (text) {
              if (onSubmit != null) onSubmit(text);
              if (nextFocusNode == null) {
                FocusScope.of(context).unfocus();
                return;
              }
              FocusScope.of(context).requestFocus(nextFocusNode);
            },
            controller: controller,
            textInputAction: textInputAction,
            textAlign: TextAlign.start,
            maxLength: maxLength,
            maxLines: maxLine,
            textAlignVertical: textAlignVertical,
            style: TextStyle(
              color: textColor,
              fontFamily: MyFont.roboto,
              fontSize: fontSize,
              fontWeight: fontWeight,
            ),
            decoration: _MyTextDecoration.textFieldStyle(
                hintTextStr: hint,
                hintColor: hintColor,
                fontSize: hintFontSize,
                fontWeight: fontWeight,
                icon: icon,
                hasFocus: focusNode.hasFocus,
                hasError: errorText != ""),
          ),
        ),
        errorText == "" ? SizedBox() : _errorSection(error: errorText),
      ],
    );
  }
}

class _MyTextDecoration {
  static InputDecoration textFieldStyle(
      {String hintTextStr,
      hintColor = MyColors.textHintColor,
      borderColor = MyColors.blackTextColor,
      bool isHitTopAlignment = false,
      FontWeight fontWeight = MyFont.regular,
      double fontSize = 16,
      bool hasFocus = false,
      Widget icon,
      bool isEnglish = false,
      bool hasError = false}) {
    return InputDecoration(
      alignLabelWithHint: isHitTopAlignment,
      hintText: hintTextStr,
      suffixIcon: icon,
      counterText: "",
      hintStyle: TextStyle(
        color: hasError
            ? MyColors.redColor
            : hasFocus ? MyColors.blackTextColor : hintColor,
        fontFamily: MyFont.roboto,
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
      labelStyle: TextStyle(
        color: hasError
            ? MyColors.redColor
            : hasFocus ? MyColors.blackTextColor : hintColor,
        fontFamily: MyFont.roboto,
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
    );
  }
}
