import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/utils/my_colors.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';

class RegularText extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final double fontSize;
  final int maxLine;
  final String fontFamily;
  final TextOverflow textOverflow;

  const RegularText({
    Key key,
    this.text = "",
    this.color = MyColors.blackTextColor,
    this.fontWeight = MyFont.medium,
    this.fontSize = 14,
    this.textAlign,
    this.maxLine,
    this.textOverflow = TextOverflow.ellipsis,
    this.fontFamily = MyFont.roboto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLine,
      textAlign: textAlign,
      overflow: textOverflow,
      style: TextStyle(
        fontSize: fontSize,
        fontFamily: fontFamily,
        fontWeight: fontWeight,
        color: color,
      ),
    );
  }
}
