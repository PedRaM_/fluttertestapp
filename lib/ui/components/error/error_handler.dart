import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/data/rest/model/main/error_body.dart';
import 'package:fluttertestsearchapp/ui/components/toast/toast.dart';

class ErrorHandler {
  final dynamic error;
  final Function logout;
  final GlobalKey<ScaffoldState> scaffoldKey;

  ErrorHandler({@required this.error, @required this.scaffoldKey, this.logout});

  void handle() {
    print(error);
    if (error is Response<dynamic>) {
      Response<dynamic> myError = error;
      ErrorBody errorBody = ErrorBody.fromJson(myError);
      Toast.showToast(scaffoldKey, message: errorBody.message);
      print(errorBody.message);
    } else if (error is String)
      Toast.showToast(scaffoldKey, message: error);
    else
      Toast.showToast(scaffoldKey, message: 'An error occurred');
  }
}
