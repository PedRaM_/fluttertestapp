import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/ui/components/text/regular_text.dart';
import 'package:fluttertestsearchapp/utils/my_colors.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';

class Toast {
  static showToast(GlobalKey<ScaffoldState> scaffoldKey,
      {@required String message, Color color = MyColors.redColor}) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: color,
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
              width: MediaQuery.of(scaffoldKey.currentContext).size.width - 85,
              child: RegularText(
                text: message,
                color: Colors.white,
                fontSize: 13,
                textAlign: TextAlign.center,
                fontWeight: MyFont.regular,
                maxLine: 2,
              )),
          SizedBox(
            width: 32,
            height: 32,
            child: InkWell(
              borderRadius: BorderRadius.circular(16),
              onTap: () {
                scaffoldKey.currentState.hideCurrentSnackBar();
              },
              child: Icon(
                Icons.close,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
