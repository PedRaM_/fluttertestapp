import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/ui/components/text/regular_text.dart';
import 'package:fluttertestsearchapp/utils/my_font.dart';

class EmptyState extends StatelessWidget {
  final String message;

  const EmptyState({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF8F8F8),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
            child: RegularText(
              text: message,
              color: Color(0xFFC2C2C2),
              fontSize: 18,
              textAlign: TextAlign.center,
              textOverflow: TextOverflow.clip,
              fontWeight: MyFont.medium,
            ),
          ),
        ],
      ),
    );
  }
}
