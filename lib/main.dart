import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertestsearchapp/config/config_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
//  if (ConfigBloc().isWhite) {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
//  } else {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarBrightness: Platform.isIOS ? Brightness.light : Brightness.dark,
    statusBarIconBrightness: Brightness.dark,
  ));
//  }

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new ConfigPage());
  });
}
