import 'dart:io';

class Tools {
  static String getFloatRate(double value) {
    if (value.roundToDouble() == value) return value.toInt().toStringAsFixed(0);
    return value.toStringAsFixed(1);
  }

  static Future<bool> chkConnectionWith({String address = 'google.com'}) async {
    try {
      final result = await InternetAddress.lookup(address);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) return true;
      return false;
    } on SocketException catch (_) {
      return false;
    }
  }
}
