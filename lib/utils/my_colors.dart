import 'package:flutter/material.dart';

class MyColors {
  static const Color textHintColor = Color(0xFF9EA6B4);
  static const Color priceColor = Color(0xFFEEF2F7);
  static const Color blackTextColor = Color(0xFF010101);
  static const Color primaryColor = Color(0xFF9253FB);
  static const Color redColor = Color(0xFFea3645);
}
