class RestUtils {
  static String getApiBaseUrl() {
    return API_BASE_URL;
  }

  static const String API_BASE_URL = "http://test.sibuy.ca/";

  static const String CATEGORY = "api/V1/categories";

  static const String CATEGORY_VIDEO = "api/V1/categories/{id}";
}
