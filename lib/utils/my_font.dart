import 'package:flutter/material.dart';

class MyFont {
  static const String roboto = "Roboto";
  static const FontWeight black = FontWeight.w600;
  static const FontWeight bold = FontWeight.w700;
  static const FontWeight light = FontWeight.w300;
  static const FontWeight medium = FontWeight.w500;
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight thin = FontWeight.w200;
}

class MyFontIcon {
  static const IconData add = IconData(0xe900, fontFamily: 'MyFontIcons');
  static const IconData avatar = IconData(0xe901, fontFamily: 'MyFontIcons');
  static const IconData download = IconData(0xe902, fontFamily: 'MyFontIcons');
  static const IconData home = IconData(0xe903, fontFamily: 'MyFontIcons');
  static const IconData search = IconData(0xe904, fontFamily: 'MyFontIcons');
  static const IconData star = IconData(0xe905, fontFamily: 'MyFontIcons');
}
