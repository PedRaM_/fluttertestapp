import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:fluttertestsearchapp/data/model/connection_problem.dart';
import 'package:fluttertestsearchapp/utils/tools.dart';

class MainPresenter {
  void callApi({
    @required Future<Response> api,
    int id,
    Function(dynamic) onFail,
    Function(dynamic) onResponseFail,
    Function onResponse,
  }) async {
    if (!await Tools.chkConnectionWith()) {
      if (onFail != null) onFail(ConnectionProblem(id: id));
      return;
    }
    api.then((response) {
      if (response.isSuccessful) {
        if (onResponse != null) onResponse(response);
        return;
      } else {
        print("id is $id and status code is ${response.statusCode}");
        if (onFail != null) onResponseFail(response);
      }
    }).catchError((error) {
      if (onFail != null) onFail(error);
    });
  }
}
